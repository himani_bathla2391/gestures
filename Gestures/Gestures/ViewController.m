//
//  ViewController.m
//  Gestures
//
//  Created by Clicklabs 104 on 10/19/15.
//  Copyright (c) 2015 cl. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *image1;

@property (weak, nonatomic) IBOutlet UIImageView *image2;
@property (weak, nonatomic) IBOutlet UIImageView *image3;

@property (weak, nonatomic) IBOutlet UIImageView *image4;

@property (weak, nonatomic) IBOutlet UIImageView *image5;
@end

@implementation ViewController
@synthesize image1;
@synthesize image2;
@synthesize image3;
@synthesize image4;
@synthesize image5;
UIImage *i1;
UIImage *i2;
UIImage *i3;
UIImage *i4;
UIImage *i5;

- (void)viewDidLoad {
    [super viewDidLoad];
    i1= [UIImage imageNamed:@"imgres-11.jpg"];
    i2= [UIImage imageNamed:@"imgres-10.jpg"];
    i3= [UIImage imageNamed:@"imgres-12.jpg"];
    [image1 setUserInteractionEnabled:YES];
    [image2 setUserInteractionEnabled:YES];
    [image3 setUserInteractionEnabled:YES];
    [image4 setUserInteractionEnabled:YES];
    [image5 setUserInteractionEnabled:YES];
//   
    UISwipeGestureRecognizer *swipeLeft = [[UISwipeGestureRecognizer alloc] initWithTarget:self action: @selector(handleSwipe:)];
    UISwipeGestureRecognizer *swipeRight = [[UISwipeGestureRecognizer alloc] initWithTarget:self action: @selector(handleSwipe:)];
    
    
    [swipeLeft setDirection:UISwipeGestureRecognizerDirectionLeft];
    [swipeRight setDirection:UISwipeGestureRecognizerDirectionRight];
    
   
    [image4 addGestureRecognizer:swipeLeft];
    [image4 addGestureRecognizer:swipeRight];
    
    
    
    
    UIPanGestureRecognizer *panGestureRecognizer = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(moveViewWithGestureRecognizer:)];
    [self.image1 addGestureRecognizer:panGestureRecognizer];
    
    
    
    
    UIPinchGestureRecognizer *pinchGestureRecognizer = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinchWithGestureRecognizer:)];
    [self.image2 addGestureRecognizer:pinchGestureRecognizer];
    
    
    
    
    UITapGestureRecognizer *singleTapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTapGesture:)];
[self.image3 addGestureRecognizer:singleTapGestureRecognizer];
    
    UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(longPress:)];
    [self.image5 addGestureRecognizer:longPress];
  
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void)moveViewWithGestureRecognizer:(UIPanGestureRecognizer *)panGestureRecognizer{
    CGPoint touchLocation = [panGestureRecognizer locationInView:self.view];
    
    self.image1.center = touchLocation;
    
}


-(void)handlePinchWithGestureRecognizer:(UIPinchGestureRecognizer *)pinchGestureRecognizer{
    pinchGestureRecognizer.scale = 2.0;
    self.image2.transform = CGAffineTransformScale(self.image2.transform, pinchGestureRecognizer.scale, pinchGestureRecognizer.scale);
    //self.image2.backgroundColor=[UIColor redColor];
    
    
}

-(void)handleSingleTapGesture:(UITapGestureRecognizer *)tapGestureRecognizer{
    CGFloat newWidth = 100.0;
    if (self.image3.frame.size.width == 139.0) {
        newWidth = 700.0;
    }
    
    CGPoint currentCenter = self.image3.center;
    
    self.image3.frame = CGRectMake(self.image3.frame.origin.x, self.image3.frame.origin.y, newWidth, self.image3.frame.size.height);
    self.image3.center = currentCenter;
}


- (void)handleSwipe:(UISwipeGestureRecognizer *)swipe {
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionLeft) {
        image4.image= [UIImage imageNamed:@"imgres-13.jpg"];
    }
    
    if (swipe.direction == UISwipeGestureRecognizerDirectionRight) {
       image4.image= [UIImage imageNamed:@"imgres-14.jpg"];
    }
    
}

- (void)longPress:(UILongPressGestureRecognizer*)gesture {
    if ( gesture.state == UIGestureRecognizerStateEnded ) {
        image5.image=[UIImage imageNamed:@"imgres-17.jpg"];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
